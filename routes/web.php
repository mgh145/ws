<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('navid', function () {
    $pusher = new Pusher\Pusher(
        'GFEDCBA',
        'PONMLKJIH',
        54321,
        config('broadcasting.connections.pusher.options')
        /*[
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'useTLS' => false
        ]*/
    );

    $pusher->trigger('test', 'test.event', \Illuminate\Support\Str::orderedUuid());

    dd(12345);

    return;
});
Route::get('tp', function () {
    broadcast(
        new \App\Http\Events\TestEvent([
            'a' => 1,
            'b' => 2
        ])
    );
});
